package com.example.demo.APIs;


public class ApiFactory {
    private static ApiFactory apiFactory;
    public static ApiFactory getInstance(){
        if (apiFactory == null){
            apiFactory = new ApiFactory();
        }
        return apiFactory;
    }

    public ApiFactory(){
        restaurantApi = RestaurantApi.getInstance();
        userApi = UserApi.getInstance();
    }


    public RestaurantApi restaurantApi;
    public UserApi userApi;
}
