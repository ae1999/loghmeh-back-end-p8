package com.example.demo.APIs;



import com.example.demo.controller.exception.*;
import com.example.demo.models.*;
import com.example.demo.repo.OrderRepo;
import com.example.demo.repo.RestaurantRepo;
import com.example.demo.repo.UserRepo;
import com.example.demo.services.FoodPartyService;
import com.example.demo.services.Runnable.GetDeliveriesRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class UserApi {

    private static UserApi userApi;
    private RestaurantRepo restaurantRepo;
    private OrderRepo orderRepo;
    private UserRepo userRepo;
    private FoodPartyService foodPartyService;
    private Map<Integer, Card> userActiveShoppingLists = new HashMap<>();

    private UserApi() {
        restaurantRepo = RestaurantRepo.getInstance();
        orderRepo = OrderRepo.getInstance();
        foodPartyService = FoodPartyService.getInstance();
        userRepo = UserRepo.getInstance();
    }

    public static UserApi getInstance(){
        if (userApi == null){
            userApi = new UserApi();
        }
        return userApi;
    }

    private double findRestaurantFoodPopularity(Restaurant restaurant){
        double sum = 0;
        int count = 0;
        List<Food> restaurantFoodList = restaurant.getMenu();
        for(Food f : restaurantFoodList){
            sum += f.getPopularity();
            count += 1;
        }
        return sum/count;
    }

    /*private ArrayList<String> findMostThreePopularRestaurants(){
        List<Restaurant> restaurants = restaurantRepo.getRestaurantList();
        Map<String,Double> restaurantsPopularity = new HashMap<>();
        for(Restaurant r : restaurants){
            double restaurantFoodPopularity = findRestaurantFoodPopularity(r);
            double distanceFromUser = r.getLocation().findDistance(this.user.getLocation());
            double popularity = restaurantFoodPopularity/distanceFromUser;
            restaurantsPopularity.put(r.getName(), popularity);
        }
        Map<String, Double> mostThreePopularRestaurantsMap = restaurantsPopularity.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Double>comparingByValue().reversed()))
                .limit(3)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        ArrayList<String> mostThreePopularRestaurants = new ArrayList<>(mostThreePopularRestaurantsMap.keySet());
        return mostThreePopularRestaurants;
    }*/



/*
    public String getMostThreePopularRestaurants(){
        ArrayList mostThreePopularRestaurants = this.findMostThreePopularRestaurants();
        JsonMapper <ArrayList> arrayListJsonMapper = new JsonMapper<>(ArrayList.class);
        return arrayListJsonMapper.toJson(mostThreePopularRestaurants);
    }
*/


    //new APIs for phase 2
    public LoghmeUser getUser(String username) {
        LoghmeUser user = userRepo.findUserByEmail(username);
        if (user == null){
            //TODO emailNotFoundException
            throw new RuntimeException();
        }
        return user;
    }

    public ArrayList<Restaurant> getNearByRestaurants(String username){
        LoghmeUser user = userRepo.findUserByEmail(username);
        if (user == null){
            //TODO emailNotFoundException
            throw new RuntimeException();
        }
        List<Restaurant> restaurants = restaurantRepo.getRestaurantList();
        ArrayList<Restaurant> filteredRestaurants = new ArrayList<>();

        for (Restaurant r : restaurants) {
            if (r.getLocation().findDistance(user.getLocation()) < 170) {
                filteredRestaurants.add(r);
            }
        }
        if (filteredRestaurants.isEmpty()){
            throw new NearByRestuarantNotfoundException();
        }
        return filteredRestaurants;
    }

    public Restaurant getRestaurant(String id,String email){
        LoghmeUser user = userRepo.findUserByEmail(email);
        if (user == null){
            //TODO emailNotFoundException(DB error Exception)
            throw new RuntimeException();
        }
        Restaurant restaurant = restaurantRepo.searchRestaurantById(id);
        if(restaurant == null){
            throw new RestaurantNotFoundException();
        }
        else if(restaurant.getLocation().findDistance(user.getLocation()) > 170){
            throw new RestaurantIsNotNearByException();
        }
        return restaurant;
    }

    public void addUserCredit(int extraCredit, String email){
        LoghmeUser user = userRepo.findUserByEmail(email);
        if (user == null){
            //TODO emailNotFoundException(DB error Exception)
            throw new RuntimeException();
        }
        user.addCredit(extraCredit);
        userRepo.updateUserCredit(user.getId(),user.getCredit());
    }

    public void addToCart(String restaurantId, String foodName, String email) {
        LoghmeUser user = userRepo.findUserByEmail(email);
        if (user == null){
            //TODO emailNotFoundException
            throw new RuntimeException();
        }
        user.setShoppingList(userActiveShoppingLists.getOrDefault(user.getId(),new Card()));
        Food food = restaurantRepo.getFoodbyName(restaurantId, foodName);
        if (food == null) {
            throw new FoodNotFoundException();
        }
        else if (!user.getShoppingList().addItem(restaurantId,food)) {
            throw new AddingFoodFromDifferentRestaurantsException();
        }
        else{
            System.out.println(food.getId());
            userActiveShoppingLists.put(user.getId(),user.getShoppingList());
        }
    }

    public void addToCartFromFoodParty(String restaurantId, String foodName,String email){
        LoghmeUser user = userRepo.findUserByEmail(email);
        if (user == null){
            //TODO emailNotFoundException
            throw new RuntimeException();
        }
        user.setShoppingList(userActiveShoppingLists.getOrDefault(user.getId(),new Card()));
        SaledFood saledFood = foodPartyService.searchFood(restaurantId,foodName);
        if(saledFood == null){
            throw new FoodNotFoundException();
        }
        else if(!saledFood.hasBeenOrdered()){
            throw new FoodPartyFoodHasBeenFinishedException();
        }
        else if(!user.getShoppingList().addItem(restaurantId, saledFood)){
            throw new AddingFoodFromDifferentRestaurantsException();
        }
        else{
            foodPartyService.updateFoodPartyFoodCount(saledFood.getId(),saledFood.getCount());
            userActiveShoppingLists.put(user.getId(),user.getShoppingList());
        }
    }

    public Card getCard(String email){
        LoghmeUser user = userRepo.findUserByEmail(email);
        if (user == null){
            //TODO emailNotFoundException(DB error Exception)
            throw new RuntimeException();
        }
        Card userShoppingList = userActiveShoppingLists.getOrDefault(user.getId(),new Card());
        return userShoppingList;
    }

    public void finalizeOrder(String email){
        LoghmeUser user = userRepo.findUserByEmail(email);
        if (user == null){
            //TODO emailNotFoundException
            throw new RuntimeException();
        }
        Card shoppingList = userActiveShoppingLists.getOrDefault(user.getId(),null);
        if(user.buy(shoppingList)){
            userActiveShoppingLists.remove(user.getId());
            CardOrder order = shoppingList.makeOrder();
            Restaurant orderRestaurant = restaurantRepo.searchRestaurantById(order.getRestaurantId());
            ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
            orderRepo.addOrder(order,scheduler,user.getId());
            System.out.println(order.getOrderId());
            scheduler.scheduleAtFixedRate(new GetDeliveriesRunnable(orderRestaurant.getLocation(),user.getLocation(),order.getOrderId()), 0, 10, TimeUnit.SECONDS);
            userRepo.updateUser(user);
        }
        else{
            throw new CreditNotEnoughException();
        }
    }

    public ArrayList<SaledFood> getFoodPartyList(){
        return foodPartyService.getFoodPartyList();
    }

    public ArrayList<CardOrder> getUserOrders(String email){
        LoghmeUser user = userRepo.findUserByEmail(email);
        if (user == null){
            //TODO emailNotFoundException
            throw new RuntimeException();
        }
        return orderRepo.getUserOrders(user.getId());
    }

    public ArrayList<Restaurant> searchRestaurant(String restaurantSearch, String foodSearch){
        ArrayList<Restaurant> restaurantsSearchResult =  restaurantRepo.searchRestaurant(restaurantSearch);
        ArrayList<Restaurant> foodSearchResults = restaurantRepo.searchRestaurantByFoodName(foodSearch);
        restaurantsSearchResult.addAll(foodSearchResults);
        return restaurantsSearchResult;

    }

    public void signUpNewUser(String email,String password, String name, String lastName){
        if (userRepo.findUserByEmail(email) == null){
            LoghmeUser loghmeUser = new LoghmeUser(0, name,lastName, "", 0,0,0, email, password);
            userRepo.addNewUser(loghmeUser);
        }
        else{
//            throw  new EmailAlreadyExistsException();
            System.out.println("somthing");
        }

    }
}
