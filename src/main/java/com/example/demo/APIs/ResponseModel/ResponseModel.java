package com.example.demo.APIs.ResponseModel;

public class ResponseModel {
    private Object object;
    private int statusCode;


    public ResponseModel(Object object, int statusCode){
        this.object = object;
        this.statusCode = statusCode;
    }

    public Object getObject() {
        System.out.println("HERE");
        return object;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
