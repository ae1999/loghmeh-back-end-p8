package com.example.demo.models;

import java.util.ArrayList;

public class LoghmeUser {
    private int id;
    private String firstName;
    private String lastName;
    private String password;
    private String phoneNumber;
    private String email;
    private Coordinate location;
    private int credit;
    private Card shoppingList;
    private ArrayList<CardOrder> orders;

    public LoghmeUser(int id, String firstName, String lastName, String phoneNumber, int x , int y, int credit, String email,String password){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.location = new Coordinate();
        this.password = password;
        location.setX(x);
        location.setY(y);
        this.credit = credit;
        shoppingList = new Card();
        orders = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public Card getShoppingList() {
        return shoppingList;
    }

    public int getCredit() {
        return credit;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail(){return email;}

    public String getPassword() {
        return password;
    }

    public Coordinate getLocation() {
        return location;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLocation(Coordinate location) {
        this.location = location;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<CardOrder> getOrders() {
        for (CardOrder o: orders){
            System.out.println(o.getOrderStatus());
        }
        return orders;
    }

    public void setShoppingList(Card shoppingList) {
        this.shoppingList = shoppingList;
    }

    public void addCredit(int extraCredit){
        credit += extraCredit;
    }

    public Boolean buy(Card shoppingList){
        if(credit >= shoppingList.getTotalPrice()){
            credit = credit - shoppingList.getTotalPrice();
            return true;
        }
        return false;
    }

    public void addOrder(CardOrder newOrder){
        orders.add(newOrder);
    }
}
