package com.example.demo.models;

import java.util.ArrayList;

public class CardOrder extends Card {
    private int orderId;
    private OrderStatus orderStatus;
    private String restaurantName;

    public CardOrder(ArrayList<CardItem> items,String restaurantId){
        this.setItems(items);
        this.setRestaurantId(restaurantId);
        this.orderStatus = OrderStatus.InSearchForDelivery;
    }

    public void setRestaurantName(String restaurantName){
        this.restaurantName = restaurantName;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderId() {
        return orderId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }


}
