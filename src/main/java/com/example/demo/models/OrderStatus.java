package com.example.demo.models;

public enum OrderStatus {
        InSearchForDelivery, DeliveryInWay, Delivered;
}

