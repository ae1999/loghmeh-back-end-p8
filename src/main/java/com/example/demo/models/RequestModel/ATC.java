package com.example.demo.models.RequestModel;

public class ATC {

    private String foodName;
    private String restaurantId;

    public String getfoodName() {
        return foodName;
    }

    public String getrestaurantId() {
        return restaurantId;
    }

    public void setfoodName(String _foodName) {
        this.foodName = _foodName;
    }

    public void setrestaurantId(String _restaurantId) {
        this.restaurantId = _restaurantId;
    }

}
