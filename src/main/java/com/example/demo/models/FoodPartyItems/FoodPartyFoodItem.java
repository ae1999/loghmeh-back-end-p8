package com.example.demo.models.FoodPartyItems;

public class FoodPartyFoodItem {
    public int count;
    public int oldPrice;
    public String name;
    public String description;
    public int price;
    public float popularity;
    public String image;
}
