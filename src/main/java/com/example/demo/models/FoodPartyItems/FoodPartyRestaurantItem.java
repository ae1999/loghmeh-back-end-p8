package com.example.demo.models.FoodPartyItems;

import com.example.demo.models.Coordinate;

import java.util.List;

public class FoodPartyRestaurantItem {
    public String id;
    public String name;
    public Coordinate location;
    public String logo;
    public List<FoodPartyFoodItem> menu;
}
