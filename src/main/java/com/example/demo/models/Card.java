package com.example.demo.models;

import java.util.ArrayList;

public class Card {
    ArrayList<CardItem> items;

    private String restaurantId;
    private int totalPrice;
    public Card(){
        items = new ArrayList<>();
    }

    private CardItem searchItem(String foodName){
        for (CardItem ci : items){
            if(ci.getFoodName().equals(foodName))
                return ci;
        }
        return null;
    }

    public void addItem(CardItem item){
        this.items.add(item);
    }

    public boolean addItem(String restaurantId,Food food){
        if (this.restaurantId == null){
            this.restaurantId = restaurantId;
        }
        if(this.restaurantId.equals(restaurantId)){
            CardItem cardItem = this.searchItem(food.getName());
            if(cardItem != null){
                cardItem.addNum();
            }
            else{
                CardItem item = new CardItem(food.getName(),1,food.getPrice());
                item.setFoodId(food.getId());
                items.add(item);
            }
            return true;
        }
        else return false;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setItems(ArrayList<CardItem> items) {
        this.items = items;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public ArrayList<CardItem> getItems() {
        return items;
    }

    public Boolean isEmpty(){
        return restaurantId == null;
    }

    public int getTotalPrice(){
        int sum =0;
        for(CardItem ci : items){
            sum += ci.getTotalPrice();
        }
        return sum;
    }

    public CardOrder makeOrder(){
        return new CardOrder(this.items,this.restaurantId);
    }
}
