package com.example.demo.models;

public class SaledFood extends Food{
    private int count;
    private int newPrice;
    private String restaurantName;
    private String restaurantId;

    public SaledFood( String name, String description, float popularity, int price, String img,int count, int newPrice, String restaurantId, String restaurantName){
        super(name, description, popularity, price,img);
        this.setCount(count);
        this.setNewPrice(newPrice);
        this.setRestaurantId(restaurantId);
        this.setRestaurantName(restaurantName);
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public int getCount() {
        return count;
    }

    public void setNewPrice(int newPrice) {
        this.newPrice = newPrice;
    }

    public int getOldPrice(){
        return super.getPrice();
    }

    @Override
    public int getPrice() {
        return newPrice;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public boolean hasBeenOrdered(){
        if (count > 0){
            count -= 1;
            return true;
        }
        else return false;
    }
}
