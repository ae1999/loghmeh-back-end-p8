package com.example.demo.models;

public class CardItem {
    int foodId;
    String foodName;
    int num;
    int price;

    public CardItem(String foodName, int num, int price){
        this.foodName = foodName;
        this.num = num;
        this.price = price;
        this.foodId = foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public int getFoodId() { return foodId;}

    public String getFoodName() {
        return foodName;
    }

    public void addNum(){
        this.num++;
    }

    public int getTotalPrice(){
        return num*price;
    }

    public int getNum() {
        return num;
    }

    public int getPrice() {
        return price;
    }

}
