package com.example.demo.models;

public class Delivery {
    String id;
    float velocity;
    Coordinate location;

    public Coordinate getLocation() {
        return location;
    }

    public String getId() {
        return id;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setLocation(Coordinate location) {
        this.location = location;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }
}
