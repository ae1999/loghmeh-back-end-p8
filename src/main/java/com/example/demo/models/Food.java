package com.example.demo.models;

public class Food {
    private int id;
    protected String name;
    private String description;
    private float popularity;
    private int price;
    private String image;

    public Food(){}
    public Food(String name, String description, float popularity, int price,String image){
        this.name = name;
        this.description = description;
        this.popularity = popularity;
        this.price = price;
        this.image = image;
    }

    public void setId(Integer id){
        this.id = id;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }
    public int getId() {
        return id;
    }
    public String getName(){
        return name;
    }
    public String getDescription(){
        return description;
    }
    public float getPopularity(){
        return popularity;
    }
    public int getPrice(){
        return price;
    }
    public String getImage() {
        return image;
    }
}
