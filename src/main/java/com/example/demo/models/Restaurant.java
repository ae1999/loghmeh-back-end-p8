package com.example.demo.models;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
    private String id;
    private String name;
    private Coordinate location;
    private String logo;
    private List<Food> menu;

    public Restaurant(String id,String name,Coordinate location,String logo){
        this.id = id;
        this.name = name;
        this.location = location;
        this.logo = logo;
        this.menu = new ArrayList<>();
    }

    public void addOneCharToId(){
        this.id = this.id + "1";
    }

    public Restaurant(){}
    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getLogo() {
        return logo;
    }

    public Coordinate getLocation() {
        return location;
    }

    public List<Food> getMenu() {
        return menu;
    }

    public void addFood(Food newFood){
        menu.add(newFood);
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setMenu(List<Food> menu) {
        this.menu = menu;
    }

    public void setLocation(Coordinate location) {
        this.location = location;
    }

}
