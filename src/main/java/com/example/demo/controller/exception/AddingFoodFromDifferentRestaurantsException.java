package com.example.demo.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "شما در سبد خرید خود سفارشی از رستوران دیگر دارید.")
public class AddingFoodFromDifferentRestaurantsException extends RuntimeException {
}
