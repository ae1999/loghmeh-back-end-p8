package com.example.demo.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "غذا یا رستوران مورد نظر وجود ندارد.")
public class FoodNotFoundException extends RuntimeException {
}
