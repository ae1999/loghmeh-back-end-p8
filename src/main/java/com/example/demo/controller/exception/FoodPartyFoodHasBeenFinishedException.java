package com.example.demo.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN,reason = "غذای مورد نظر به اتمام رسیده است.")
public class FoodPartyFoodHasBeenFinishedException extends RuntimeException {
}
