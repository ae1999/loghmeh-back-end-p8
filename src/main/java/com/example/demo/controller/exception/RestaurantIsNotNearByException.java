package com.example.demo.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "این رستوران در محدوده ی شما قرار ندارد.")
public class RestaurantIsNotNearByException extends RuntimeException{
}
