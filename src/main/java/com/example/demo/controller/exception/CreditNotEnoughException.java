package com.example.demo.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "اعتبار شما برای خرید کافی نیست.")
public class CreditNotEnoughException extends RuntimeException {
}
