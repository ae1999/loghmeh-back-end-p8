package com.example.demo.controller;


import com.example.demo.APIs.ApiFactory;
import com.example.demo.models.RequestModel.ATC;
import com.example.demo.models.SaledFood;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
public class FoodPartyController {
    private ApiFactory apiFactory;
    public FoodPartyController(){
        apiFactory = ApiFactory.getInstance();
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/food_party", method = RequestMethod.GET)
    public ArrayList<SaledFood> getRestaurants(){
        return  apiFactory.userApi.getFoodPartyList();
    }

    @ResponseStatus(value = HttpStatus.OK,reason = "غذا با موفقیت به سبد خرید اضافه شد.")
    @RequestMapping(value = "/food_party/add_to_cart",method = RequestMethod.POST)
    public void addFoodToCardFromFoodParty(@RequestBody ATC atc,
            Authentication authentication){
        apiFactory.userApi.addToCartFromFoodParty(atc.getrestaurantId(), atc.getfoodName(), authentication.getName());

    }
}