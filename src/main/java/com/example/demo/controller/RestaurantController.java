package com.example.demo.controller;


import com.example.demo.APIs.ApiFactory;
import com.example.demo.models.Restaurant;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;


@RestController
public class RestaurantController {
    private ApiFactory apiFactory;
    public RestaurantController(){
        apiFactory = ApiFactory.getInstance();
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "restaurants", method = RequestMethod.GET)
    public ArrayList<Restaurant> getRestaurants(@RequestParam Map< String, String> queryParameters, Authentication authentication){
        ArrayList<Restaurant> restaurants;
        if(queryParameters.containsKey("restaurant") || queryParameters.containsKey("food")){
            restaurants = apiFactory.userApi.searchRestaurant(queryParameters.getOrDefault("restaurant", ""),
                    queryParameters.getOrDefault("food", ""));
        }
        else {
            restaurants = apiFactory.userApi.getNearByRestaurants(authentication.getName());
        }
        return restaurants;
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/restaurants/{id}", method = RequestMethod.GET)
    public Restaurant getRestaurant(@PathVariable("id") String id, Authentication authentication){
        return apiFactory.userApi.getRestaurant(id,authentication.getName());
    }




}
