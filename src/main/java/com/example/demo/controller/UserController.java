package com.example.demo.controller;

import com.example.demo.APIs.ApiFactory;
import com.example.demo.models.Card;
import com.example.demo.models.CardOrder;
import com.example.demo.models.LoghmeUser;
import com.example.demo.models.RequestModel.ATC;
import com.example.demo.models.RequestModel.Credit;
import com.example.demo.models.RequestModel.UserRegisterInfo;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@RequestMapping("/users")
public class UserController {
    private ApiFactory apiFactory;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    public UserController(BCryptPasswordEncoder bCryptPasswordEncoder){
        apiFactory = ApiFactory.getInstance();
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public void SignUp(@RequestBody UserRegisterInfo user){
        System.out.println(user.getPassword());
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        apiFactory.userApi.signUpNewUser(user.getEmail(), encodedPassword, user.getName(), user.getLastName());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public LoghmeUser getUser(Authentication authentication){
        String username = authentication.getName();
        return apiFactory.userApi.getUser(username);
    }

    @ResponseStatus(value = HttpStatus.OK, reason = "اعتبار با موفقیت افزوده شد.")
    @RequestMapping(value = "/add_credit", method = RequestMethod.POST)
    public void addCredit(@RequestBody Credit credit, Authentication authentication){
        System.out.println(credit.getcredit());
        apiFactory.userApi.addUserCredit(credit.getcredit() ,authentication.getName());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public Card getUserCard(Authentication authentication){
        return apiFactory.userApi.getCard(authentication.getName());
    }

    @ResponseStatus(value = HttpStatus.OK,reason = "غذا با موفقیت به سبد خرید اضافه شد.")
    @RequestMapping(value = "/add_to_cart",method = RequestMethod.POST)
    public void addFoodToCard(@RequestBody ATC atc, Authentication authentication) {
        apiFactory.userApi.addToCart(atc.getrestaurantId(),atc.getfoodName(),authentication.getName());

    }

    @ResponseStatus(value = HttpStatus.OK, reason = "سفارش با موفقیت ثبت شد.")
    @RequestMapping(value = "/finalize_order", method = RequestMethod.GET)
    public void finalizeOrder(Authentication authentication){
        apiFactory.userApi.finalizeOrder(authentication.getName());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public ArrayList<CardOrder> getUserOrders(Authentication authentication){
        return apiFactory.userApi.getUserOrders(authentication.getName());
    }
}
