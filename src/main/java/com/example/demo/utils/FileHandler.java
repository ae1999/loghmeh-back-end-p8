package com.example.demo.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileHandler {
    public String readFromFile(String fileName){
        String text;
        text = "";
        try{
            File testInputFile = new File(getClass().getClassLoader().getResource(fileName).getFile());
            Scanner sc = new Scanner(testInputFile);
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                text += line;
                text += '\n';
            }
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        return text.trim();
    }
}
