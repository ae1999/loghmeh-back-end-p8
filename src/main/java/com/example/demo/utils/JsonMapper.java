package com.example.demo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class JsonMapper<T> {
    private ObjectMapper mapper;
    private Class<T> className;
    public JsonMapper(Class<T> className){
        mapper = new ObjectMapper();
        this.className = className;
    }
    public T readJson(String json){
        Object obj = null;
        try {
            obj =  mapper.readValue(json, className);
        }
        catch (JsonProcessingException e){
            System.out.println(e.getMessage());
        }
        return (T) obj;
    }

    public String toJson(T t){
        String jsonString = "";
        try{
            jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(t);
        }
        catch(JsonProcessingException e){
            System.out.println(e.getMessage());
        }
        return jsonString;
    }

    public<F> List<F> readJsonList(Class<F> className,String json){
        List list = null;
        List<F> finalList = new ArrayList<>();
        try {
            list =  mapper.readValue(json, List.class);
            for (Object r : list){
                finalList.add(mapper.convertValue(r,className));
            }
        }
        catch (JsonProcessingException e){
            System.out.println(e.getMessage());
        }
        return finalList;
    }


}
