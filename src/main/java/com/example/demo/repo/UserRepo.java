package com.example.demo.repo;


import com.example.demo.dataAccess.UserMapper;
import com.example.demo.models.LoghmeUser;

import java.sql.SQLException;

public class UserRepo {
    private UserMapper userMapper = UserMapper.getInstance();
    private static UserRepo userRepo = new UserRepo();



    public static UserRepo getInstance(){
        return userRepo;
    }

    public LoghmeUser findUserByEmail(String email){
        LoghmeUser loghmeUser = null;
        try{
            loghmeUser = userMapper.findByEmail(email);
        } catch (SQLException e){
            e.printStackTrace();
        }
        return loghmeUser;
    }

    public void addNewUser(LoghmeUser loghmeUser){
        try {
            userMapper.insert(loghmeUser);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updateUserCredit(int userId,int newCredit){
        try{
            userMapper.updateUserCredit(userId,newCredit);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updateUser(LoghmeUser user){
        try{
            userMapper.updateUser(user);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
