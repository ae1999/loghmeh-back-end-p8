package com.example.demo.repo.APICaller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class APICaller {

    public String getData(String urlString) {
        String data = "";
        try {
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                data += inputLine;
                data += "\n";
            }
            data = data.trim();
            in.close();
        }
        catch (IOException  e){
            System.out.println(e.getMessage());
        }
        return data;
    }
}
