package com.example.demo.repo;

import com.example.demo.dataAccess.OrderItemMapper;
import com.example.demo.dataAccess.OrderMapper;
import com.example.demo.models.CardItem;
import com.example.demo.models.CardOrder;
import com.example.demo.models.Delivery;
import com.example.demo.models.OrderStatus;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;

public class OrderRepo {
    private static OrderRepo orderRepo;
    /* private List<CardOrder> orders;*/
    private Map<Integer, Delivery> orderDeliveryMap;
    private Map<Integer, ScheduledExecutorService> orderSchedulerMap;

    private OrderMapper orderMapper = OrderMapper.getInstance();
    private OrderItemMapper orderItemMapper = OrderItemMapper.getInstance();
    public static OrderRepo getInstance(){
        if (orderRepo == null){
            orderRepo = new OrderRepo();
        }
        return orderRepo;
    }

    private OrderRepo(){
        orderDeliveryMap = new HashMap<>();
        orderSchedulerMap = new HashMap<>();
    }
    public void addOrder(CardOrder newOrder, ScheduledExecutorService scheduler, Integer userId) {
        try {
            int orderId = orderMapper.insertAndGetId(newOrder, userId, newOrder.getRestaurantId());
            newOrder.setOrderId(orderId);
            for (CardItem item: newOrder.getItems()){
                orderItemMapper.insert(item,item.getFoodId(),orderId);
            }
            newOrder.setOrderId(orderId);
        } catch (SQLException e){
            e.printStackTrace();
        }
        orderSchedulerMap.put(newOrder.getOrderId(),scheduler);
    }
    public CardOrder searchOrdersById(int orderId){
        CardOrder order = null;
        try{
            order = orderMapper.find(orderId);
/*            if (order!=null){
                ArrayList<CardItem > items = orderItemMapper.retrieveOrderItems(orderId);
                order.setItems(items);
            }*/
        } catch (SQLException e){
            e.printStackTrace();
        }
        return order;
    }
    public void assignDeliveryToOrder(int orderId, Delivery delivery,long deliveryTime){
        CardOrder order = searchOrdersById(orderId);
        System.out.println(order);
        orderDeliveryMap.put(orderId,delivery);
        try {
            orderMapper.updateOrderStatus(OrderStatus.DeliveryInWay, orderId);
        } catch (SQLException e){
            e.printStackTrace();
        }
        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            orderMapper.updateOrderStatus(OrderStatus.Delivered,orderId);
                        } catch (SQLException e){
                            e.printStackTrace();
                        }
                    }
                },
                deliveryTime
        );
    }

    public Map<Integer, ScheduledExecutorService> getOrderSchedulerMap() {
        return orderSchedulerMap;
    }

    public void deleteFromOrderScheduledMap(int orderId){
        orderSchedulerMap.remove(orderId);
    }

    public ArrayList<CardOrder> getUserOrders(int userId){
        ArrayList<CardOrder> orders = new ArrayList<>();
        try{
            orders = orderMapper.retrieveUserOrders(userId);
            for (CardOrder o: orders){
                ArrayList<CardItem> items = orderItemMapper.retrieveOrderItems(o.getOrderId());
                o.setItems(items);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return orders;
    }

}
