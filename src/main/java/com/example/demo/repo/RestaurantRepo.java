package com.example.demo.repo;

import com.example.demo.dataAccess.FoodMapper;
import com.example.demo.dataAccess.RestaurantMapper;
import com.example.demo.models.Food;
import com.example.demo.models.Restaurant;
import com.example.demo.repo.APICaller.APICaller;
import com.example.demo.utils.JsonMapper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class RestaurantRepo {
    private static RestaurantRepo restaurantRepo;

    private RestaurantMapper restaurantMapper = RestaurantMapper.getInstance();
    private FoodMapper foodMapper = FoodMapper.getInstance();

    public static RestaurantRepo getInstance(){
        if (restaurantRepo == null){
            restaurantRepo = new RestaurantRepo();
        }
        return restaurantRepo;
    }

    public void getRestuarantsFromAPI(){
        APICaller apiCaller = new APICaller();
        JsonMapper<List> listJsonMapper= new JsonMapper<List>(List.class);
        List<Restaurant> restaurantList = listJsonMapper.readJsonList(Restaurant.class,apiCaller.getData("http://138.197.181.131:8080/restaurants"));
        System.out.println(listJsonMapper.toJson(restaurantList));
        saveNewRestaurants(restaurantList);
    }

    private void saveNewRestaurants(List<Restaurant> restaurants){
        try{

            for(Restaurant r: restaurants){
                if (restaurantMapper.find(r.getId()) == null){
                    restaurantMapper.insert(r);
                }
                saveRestaurantFoods(r.getMenu(), r.getId());
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void saveRestaurantFoods(List<Food> foods, String restaurantId){
        try{
            for (Food f: foods){
                foodMapper.insert(f,restaurantId);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void addRestaurant(Restaurant newRestaurant){
        try{
            Restaurant restaurant = restaurantMapper.find(newRestaurant.getId());
            if (restaurant == null){
                restaurantMapper.insert(newRestaurant);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void addFood(Food newFood, String restaurantId){
        Food food = null;
        try{
            food = foodMapper.findFoodByName(newFood.getName(),restaurantId);
            if (food == null){
                foodMapper.insert(newFood,restaurantId);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }

    }

    public ArrayList<Restaurant> searchRestaurantByFoodName(String search){
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        if (search.equals("")){
            return restaurants;
        }
        try{
            restaurants = foodMapper.searchFoodByName(search);
        } catch (SQLException e){
            e.printStackTrace();
        }
        return restaurants;
    }


    public ArrayList<Restaurant> searchRestaurant(String restaurantName){
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        if (restaurantName.equals("")){
            return restaurants;
        }
        try{
            restaurants = restaurantMapper.searchRestaurantByName(restaurantName);
        } catch (SQLException e){
            e.printStackTrace();
        }
        return restaurants;
    }

    public Food getFoodbyName(String restaurantId, String foodName){
        Food food = null;
        try{
            food = foodMapper.findFoodByName(foodName,restaurantId);

        } catch (SQLException e){
            e.printStackTrace();
        }
        return food;
    }



    public List<Restaurant> getRestaurantList() {
        ArrayList<Restaurant> restaurants = null;
        try{
            restaurants = restaurantMapper.retrieveAll();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return restaurants;
    }

    public Restaurant searchRestaurantById(String id){
        Restaurant restaurant = null;
        try{
            restaurant = restaurantMapper.find(id);
            if (restaurant!=null) {
                ArrayList<Food> foods = foodMapper.retrieveRestaurantFood(id);
                for (Food f : foods) {
                    restaurant.addFood(f);
                }
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return restaurant;
    }



}
