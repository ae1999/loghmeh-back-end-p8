package com.example.demo.dataAccess;

import com.example.demo.models.Coordinate;
import com.example.demo.models.Restaurant;

import java.sql.*;
import java.util.ArrayList;

public class RestaurantMapper extends IndependentMapper<Restaurant, String>{

    private static final String COLUMNS = " id, name, loc_x, loc_y, logo";
    private static final String TABLE_NAME = "restaurant";

    private static RestaurantMapper restaurantMapper = new RestaurantMapper();

    public static RestaurantMapper getInstance(){
        return restaurantMapper;
    }

    private RestaurantMapper(){
        try(Connection conn = ConnectionPool.getConnection()){
            Statement st = conn.createStatement();
            st.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s " +
                            "(" +
                            "`id` char(25) NOT NULL, " +
                            "`name` varchar(100) NOT NULL, " +
                            "`loc_x` int(11) NOT NULL, " +
                            "`loc_y` int(11) NOT NULL, " +
                            "`logo` varchar(1000) DEFAULT NULL," +
                            "PRIMARY KEY (`id`) " +
                            ")CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
                    TABLE_NAME));
        } catch (SQLException ex){
            System.out.println("Error in RestaurantMapper.RestaurantMapper");
            ex.printStackTrace();
        }
    }

    @Override
    protected String getFindStatement(String id){
        return "SELECT " + COLUMNS +
                " FROM " + TABLE_NAME +
                " WHERE id = " + '"' + id + '"' + ";";
    }

    @Override
    protected String getInsertStatement(Restaurant restaurant) {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" + " VALUES "+
                "("+
                '"' +  restaurant.getId() + '"' + "," +
                '"' + restaurant.getName() + '"'+ "," +
                restaurant.getLocation().getX() + "," +
                restaurant.getLocation().getY() + "," +
                '"' +  restaurant.getLogo() + '"' +
                ");";
    }

    @Override
    protected String getDeleteStatement(String id) {
        return "DELETE FROM " + TABLE_NAME + " WHERE id = " + id + ";";
    }

    @Override
    protected Restaurant convertResultSetToObject(ResultSet rs) throws SQLException {
        Restaurant newRestaurant = null;
        try{
            newRestaurant = new Restaurant(rs.getString(1),
                    rs.getString(2),
                    new Coordinate(rs.getInt(3),rs.getInt(4)) ,
                    rs.getString(5));
        } catch (SQLException e){
            System.out.println("Error in RestaurantMapper.convertResultSetToObject");
            throw e;
        }
        return newRestaurant;
    }



    public ArrayList<Restaurant> retrieveAll() throws SQLException{
        String sql = "SELECT * FROM " + TABLE_NAME;
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet resultSet = st.executeQuery();
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            while(resultSet.next()){
                Restaurant restaurant = convertResultSetToObject(resultSet);
                restaurants.add(restaurant);
            }
            return restaurants;
        } catch (SQLException e){
            System.out.println("Error in retrieve restaurants.");
            throw e;
        }
    }

    public ArrayList<Restaurant> searchRestaurantByName(String search)throws SQLException{
        String sql = "SELECT DISTINCT " + COLUMNS + " FROM " + TABLE_NAME + " WHERE 'name' LIKE " + "\"%" + search + "%\"";
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            while (rs.next()){
                restaurants.add(this.convertResultSetToObject(rs));
                System.out.println("test");
            }
            return restaurants;
        } catch (SQLException e){
            System.out.println("Error in FoodMapper.searchFoodByName query");
            throw e;
        }
    }
}

