package com.example.demo.dataAccess;


import com.example.demo.models.Food;
import com.example.demo.models.Restaurant;

import java.sql.*;
import java.util.ArrayList;

public class FoodMapper extends DependentToOneMapper<Food, String, Integer> {

    private final String COLUMNS = "name, description, popularity, price, image, res_id";
    private final String COLUMNS_WITH_ID = " id, " + COLUMNS;
    private final String TABLE_NAME = "food";

    private static FoodMapper foodMapper = new FoodMapper();
    public static FoodMapper getInstance(){
        return foodMapper;
    }

    private FoodMapper(){
        try(Connection conn = ConnectionPool.getConnection()){
            Statement st = conn.createStatement();
            st.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS  %s " +
                            "(" +
                            "`id` int(11) NOT NULL AUTO_INCREMENT, " +
                            "`name` varchar(100) NOT NULL, " +
                            "`description` text, " +
                            "`price` int(11) NOT NULL, " +
                            "`popularity` float DEFAULT NULL, " +
                            "`res_id` char(25) NOT NULL, " +
                            "`image` varchar(10000)," +
                            "PRIMARY KEY (`id`), " +
                            "KEY `res_id` (`res_id`), " +
                            "CONSTRAINT `res_id` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE " +
                            ")CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
                    TABLE_NAME));
        } catch (SQLException ex){
            System.out.println("Error in FoodMapper.FoodMapper");
            ex.printStackTrace();
        }
    }

    @Override
    protected String getFindStatement(Integer id) {
        return "SELECT " + COLUMNS_WITH_ID +
                " FROM " + TABLE_NAME +
                " WHERE id = " + id.toString() + ";";
    }



    @Override
    protected String getInsertStatement(Food food, String restaurantId) {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" + " VALUES "+
                "("+
                '"' +  food.getName() + '"' + ", " +
                '"' + food.getDescription() + '"'+ ", " +
                food.getPopularity() + ", " +
                food.getPrice() + "," +
                '"' + food.getImage() + '"' + ", " +
                '"' + restaurantId + '"' +
                ");";
    }

    @Override
    protected String getDeleteStatement(Integer id) {
        return "DELETE FROM " + TABLE_NAME + " WHERE id = " + id + ";";
    }

    @Override
    protected Food convertResultSetToObject(ResultSet rs) throws SQLException {
        Food newFood = null;
        try {
            newFood = new Food(rs.getString(2),
                    rs.getString(3),
                    rs.getFloat(4),
                    rs.getInt(5),
                    rs.getString(6));
            newFood.setId(rs.getInt(1));
        } catch (SQLException e){
            System.out.println("Error in FoodMapper.convertResultSetToObject");
            throw e;
        }
        return newFood;
    }

    public Food findFoodByName(String foodName, String restaurantId) throws SQLException{
        String sql = "SELECT " + COLUMNS_WITH_ID +  " FROM " + TABLE_NAME + " WHERE res_id = " + '"' + restaurantId + '"' + " and name = " + '"' + foodName + '"' + ';';
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            System.out.println(st);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            return convertResultSetToObject(resultSet);
        } catch (SQLException e){
            System.out.println("error in FoodMapper.findByName query.");
            throw e;
        }
    }

    public ArrayList<Food> retrieveRestaurantFood(String restaurantId) throws SQLException{
        String sql = "SELECT " + COLUMNS_WITH_ID +  " FROM " + TABLE_NAME + " WHERE res_id = " + '"' + restaurantId + '"'+ ";";
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            System.out.println(st);
            ResultSet resultSet = st.executeQuery();
            ArrayList<Food> foods = new ArrayList<>();
            while(resultSet.next()){
                foods.add(convertResultSetToObject(resultSet));
            }
            return foods;
        } catch (SQLException e){
            System.out.println("Error in retrieve restaurant's foods.");
            throw e;
        }
    }

    public  ArrayList<Restaurant>  searchFoodByName(String search) throws SQLException{
        String sql = "SELECT DISTINCT res_id FROM " + "food" + " WHERE name LIKE " + "\"%" + search + "%\"" +";";
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            while (rs.next()){
                restaurants.add(RestaurantMapper.getInstance().find(rs.getString(1)));
            }
            return restaurants;
        } catch (SQLException e){
            System.out.println("Error in FoodMapper.searchFoodByName query");
            throw e;
        }
    }

}
