package com.example.demo.dataAccess;


import com.example.demo.models.CardItem;
import com.example.demo.models.CardOrder;
import com.example.demo.models.OrderStatus;

import java.sql.*;
import java.util.ArrayList;

public class OrderMapper extends DependentToTwoMapper<CardOrder, Integer, String, Integer> {
    private final String COLUMNS = " user_id, res_id, order_status";
    private final String COLUMNS_WITH_ID = " id," + COLUMNS;
    private final String TABLE_NAME = "`order`";

    private static OrderMapper orderMapper = new OrderMapper();

    public static OrderMapper getInstance(){
        return orderMapper;
    }

    private OrderMapper(){
        try(Connection conn = ConnectionPool.getConnection()){
            Statement st = conn.createStatement();
            st.executeUpdate("CREATE TABLE IF NOT EXISTS `order` (" +
                    "  `id` int(11) NOT NULL AUTO_INCREMENT," +
                    "  `user_id` int(11) NOT NULL," +
                    "  `res_id` char(25) NOT NULL," +
                    "  `order_status` enum('InSearchForDelivery','DeliveryInWay','Delivered') NOT NULL,\n" +
                    "  PRIMARY KEY (`id`)," +
                    "  KEY `id_idx` (`user_id`)," +
                    "  KEY `res_id_order` (`res_id`)," +
                    "  CONSTRAINT `res_id_order` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION," +
                    "  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION" +
                    ")CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
            );
            System.out.println();
        } catch (SQLException ex){
            System.out.println("Error in OrderMapper.OrderMapper");
            ex.printStackTrace();
        }
    }



    @Override
    protected String getFindStatement(Integer id) {
        return "SELECT " + " O.id, O.user_id, O.res_id, O.order_status, R.name" +
                " FROM " + TABLE_NAME + " O , restaurant R " +
                " WHERE O.id = " + id + " and res_id = R.id";
    }

    @Override
    protected String getInsertStatement(CardOrder cardOrder, Integer userId, String restaurantId) {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" + " VALUES "+
                "("+
                userId + "," +
                '"' + restaurantId + '"'+ "," +
                '"' + cardOrder.getOrderStatus() + '"' +
                ");";
    }

    @Override
    protected String getDeleteStatement(Integer id) {
        return "DELETE FROM " + TABLE_NAME + " WHERE id = " + id + ";";
    }

    @Override
    protected CardOrder convertResultSetToObject(ResultSet rs) throws SQLException {
        CardOrder cardOrder = new CardOrder(new ArrayList<CardItem>(), rs.getString(3));
        cardOrder.setOrderId(rs.getInt(1));
        cardOrder.setOrderStatus(OrderStatus.valueOf(rs.getString(4)));
        cardOrder.setRestaurantName(rs.getString(5));
        return cardOrder;
    }

    public int insertAndGetId(CardOrder cardOrder, Integer userId, String restaurantId) throws SQLException{
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getInsertStatement(cardOrder, userId, restaurantId), Statement.RETURN_GENERATED_KEYS)
        ) {
            try {
                st.executeUpdate();
                ResultSet rs = st.getGeneratedKeys();
                int generatedKey = -1;
                if (rs.next()){
                    generatedKey = rs.getInt(1);
                }
                return generatedKey;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.insert query.");
                throw ex;
            }
        }
    }

    public void updateOrderStatus(OrderStatus os, Integer orderId) throws SQLException {
        String sql = "UPDATE " + TABLE_NAME + "SET order_status = " + '"' + os.toString() + '"' + " WHERE id = " + orderId;
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement statement = conn.prepareStatement(sql);
            System.out.println(statement);
            statement.executeUpdate();
        } catch (SQLException e){
            System.out.println("error in Mapper.udpateOrderStatus query.");
            throw e;
        }
    }

    public ArrayList<CardOrder> retrieveUserOrders(Integer userId) throws SQLException{
        String sql = "SELECT " + " O.id, O.user_id, O.res_id, O.order_status, R.name" +
                " FROM " + TABLE_NAME + " O , restaurant R " +
                " WHERE user_id = " + userId + " and res_id = R.id";
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            ArrayList<CardOrder> orders = new ArrayList<>();
            while (rs.next()){
                orders.add(this.convertResultSetToObject(rs));
            }
            return orders;
        } catch (SQLException e){
            System.out.println("Error in FoodMapper.searchFoodByName query");
            throw e;
        }
    }

}
