package com.example.demo.dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class DependentToTwoMapper<T,F,Z,I>  extends Mapper<T,I>{

    abstract protected String getInsertStatement(T t, F f, Z z);

    public void insert(T obj, F f, Z z) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getInsertStatement(obj,f,z))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.insert query.");
                throw ex;
            }
        }
    }

}
