package com.example.demo.dataAccess;

import com.example.demo.models.CardItem;

import java.sql.*;
import java.util.ArrayList;

public class OrderItemMapper extends DependentToTwoMapper<CardItem, Integer, Integer, Integer> {
    private final String COLUMNS = " food_id, num, order_id";
    private final String COLUMNS_WITH_ID = " id," + COLUMNS;
    private final String TABLE_NAME = "order_item";
    private final String FATHER_TABLE = "food";
    private final String FATHER_COLUMNS = "name, price ";

    private static OrderItemMapper orderItemMapper = new OrderItemMapper();
    public static OrderItemMapper getInstance(){
        return orderItemMapper;
    }
    private OrderItemMapper(){
        try(Connection conn = ConnectionPool.getConnection()){
            Statement st = conn.createStatement();
            st.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s " +
                            "(" +
                            " `id` int(11) NOT NULL AUTO_INCREMENT, " +
                            "`food_id` int(11) DEFAULT NULL, " +
                            "`num` int(11) DEFAULT NULL, " +
                            "`order_id` int(11) NOT NULL, " +
                            "PRIMARY KEY (`id`)," +
                            "CONSTRAINT `food_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`) ON DELETE SET NULL ON UPDATE CASCADE, " +
                            "CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE" +
                            ")CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
                    TABLE_NAME));
        } catch (SQLException ex){
            System.out.println("Error in OrderItemMapper.OrderItemMapper");
            ex.printStackTrace();
        }
    }


    @Override
    protected String getFindStatement(Integer orderId) {
        return "SELECT " + COLUMNS + ", " + FATHER_COLUMNS +
                " FROM " + TABLE_NAME + " O, " + FATHER_TABLE + " F" +
                " WHERE O.order_id = " + orderId + " AND F.id = O.food_id" + ";";
    }

    @Override
    protected String getInsertStatement(CardItem cardItem, Integer foodId, Integer orderId) {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" + " VALUES "+
                "("+
                foodId + "," +
                cardItem.getNum() + "," +
                orderId +
                ");";
    }

    @Override
    protected String getDeleteStatement(Integer id) {
        return "DELETE FROM " + TABLE_NAME + " WHERE id = " + id + ";";
    }

    @Override
    protected CardItem convertResultSetToObject(ResultSet rs) throws SQLException {
        CardItem item = new CardItem(rs.getString(4),
                rs.getInt(2),
                rs.getInt(5)
        );
        item.setFoodId(rs.getInt(1));
        return item;
    }

    public ArrayList<CardItem> retrieveOrderItems(Integer orderId) throws SQLException{
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getFindStatement(orderId))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                ArrayList<CardItem> items = new ArrayList<>();
                while(resultSet.next()){
                    items.add(this.convertResultSetToObject(resultSet));
                }
                return items;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.retrieveOrderItems query.");
                throw ex;
            }
        }
    }
}
