package com.example.demo.dataAccess;

import com.example.demo.models.LoghmeUser;

import java.sql.*;


public class UserMapper extends IndependentMapper<LoghmeUser,Integer> {
    private static final String COLUMNS = " first_name, last_name, phone_number, loc_x, loc_y , credit, email, password ";
    private static final String COLUMNS_WITH_ID = " id, " + COLUMNS;
    private static final String TABLE_NAME = "user";

    private static UserMapper userMapper = new UserMapper();

    public static UserMapper getInstance(){
        return userMapper;
    }

    private UserMapper(){
        try(Connection conn = ConnectionPool.getConnection()){
            Statement st = conn.createStatement();
            st.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS %s " +
                            "(" +
                            "`id` int(11) NOT NULL AUTO_INCREMENT, " +
                            "`first_name` varchar(100) NOT NULL," +
                            "`last_name` varchar(100) NOT NULL, " +
                            "`phone_number` char(11) NOT NULL, " +
                            " `loc_x` int(11) NOT NULL," +
                            "`loc_y` int(11) NOT NULL," +
                            "`credit` int(11) NOT NULL, " +
                            "`email` varchar(100) NOT NULL, " +
                            "`password` varchar(100) NOT NULL, " +
                            "UNIQUE INDEX `email_UNIQUE` (`email` ASC)," +
                            "PRIMARY KEY (`id`)" +
                            ")CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
                    TABLE_NAME));
        } catch (SQLException ex){
            System.out.println("Error in UserMapper.UserMapper");
            ex.printStackTrace();
        }
    }

    @Override
    protected String getFindStatement(Integer id){
        return "SELECT " + COLUMNS_WITH_ID +
                " FROM " + TABLE_NAME +
                " WHERE id = " + id.toString() + ";";
    }

    @Override
    protected String getInsertStatement(LoghmeUser user){
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" + " VALUES "+
                "("+
                '"' +  user.getFirstName() + '"' + "," +
                '"' + user.getLastName() + '"'+ "," +
                '"' + user.getPhoneNumber() + '"' + "," +
                user.getLocation().getX() + "," +
                user.getLocation().getY() + "," +
                user.getCredit() + "," +
                '"' + user.getEmail() + '"' + ',' +
                '"' + user.getPassword() + '"' +
                ");";
    }

    @Override
    protected String getDeleteStatement(Integer id){
        return "DELETE FROM " + TABLE_NAME + " WHERE id = " + id.toString() + ";";
    }

    @Override
    protected LoghmeUser convertResultSetToObject(ResultSet rs) throws SQLException{
        return new LoghmeUser(rs.getInt(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getInt(5), rs.getInt(6),
                rs.getInt(7),
                rs.getString(8),
                rs.getString(9));
    }

    protected String getFindByEmailStatement(String email){
        return "SELECT " + COLUMNS_WITH_ID +
                " FROM " + TABLE_NAME +
                " WHERE email = " + '"' + email + '"' + ";";
    }

    public LoghmeUser findByEmail(String email) throws SQLException{
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getFindByEmailStatement(email))
        ) {
            System.out.println(st);
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if (resultSet.next()){
                    return convertResultSetToObject(resultSet);
                }
                else return null;
            } catch (SQLException ex) {
                System.out.println("error in UserMapper.findByEmail query.");
                throw ex;
            }
        }
    }

    public void updateUserCredit(int id, int newCredit) throws SQLException{
        String sql = "UPDATE " + TABLE_NAME + " SET credit = " + newCredit + " WHERE id = " + id;
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.executeUpdate();
            System.out.println("okokok");
        } catch (SQLException e){
            System.out.println("error in UserMapper.updateUserCredit query.");
            throw e;
        }
    }

    public void updateUser(LoghmeUser user) throws SQLException{
        String sql = "UPDATE " + TABLE_NAME + " SET " + "first_name = " + '"' + user.getFirstName() + '"' +
                ", last_name = " + '"' +  user.getLastName() + '"' + ", phone_number = " + '"' + user.getPhoneNumber() + '"' +
                ", credit = " + user.getCredit() + ", loc_x = " + user.getLocation().getX() + ", loc_y = " + user.getLocation().getY();
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.executeUpdate();
        } catch (SQLException e){
            System.out.println("error in UserMapper.updateUserCredit query.");
            throw e;
        }
    }
}

