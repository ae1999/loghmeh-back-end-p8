package com.example.demo.dataAccess;

import org.apache.commons.dbcp.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {
    private static BasicDataSource ds = new BasicDataSource();
    static {
//        develoment
//        ds.setDriverClassName("com.mysql.jdbc.Driver");
//        ds.setUrl("jdbc:mysql://localhost:8889/LoghmeDB?useUnicode=yes&characterEncoding=UTF-8");
//        ds.setUsername("root");
//        ds.setPassword("root");
//        ds.setMinIdle(1);
//        ds.setMaxIdle(5);
//        ds.setMaxOpenPreparedStatements(100);
//        deployment
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://loghme-mysql:3306/LoghmeDB?useUnicode=yes&characterEncoding=UTF-8");
        ds.setUsername("test");
        ds.setPassword("123456");
        ds.setMinIdle(1);
        ds.setMaxIdle(5);
        ds.setMaxOpenPreparedStatements(100);
    }

    public static Connection getConnection() throws SQLException{
        return ds.getConnection();
    }

    private ConnectionPool(){}
}
