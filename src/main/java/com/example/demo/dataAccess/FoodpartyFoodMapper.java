package com.example.demo.dataAccess;

import com.example.demo.models.SaledFood;

import java.sql.*;
import java.util.ArrayList;

public class FoodpartyFoodMapper extends DependentToOneMapper<SaledFood, String, Integer> {
    private final String COLUMNS = " food_id, count, new_price ";
    private final String TABLE_NAME = "foodparty_food";
    private final String FATHER_COLUMNS = " name, description, image, price, popularity, res_id";
    private final String FATHER_COLUMNS_WITH_ID = " id," + FATHER_COLUMNS;
    private final String FATHER_TABLE_NAME = "food";


    private static FoodpartyFoodMapper foodpartyFoodMapper = new FoodpartyFoodMapper();

    public static FoodpartyFoodMapper getInstance(){
        return foodpartyFoodMapper;
    }

    private FoodpartyFoodMapper(){
        try(Connection conn = ConnectionPool.getConnection()){
            Statement st = conn.createStatement();
            st.executeUpdate(String.format("CREATE TABLE IF NOT EXISTS  %s " +
                            "(" +
                            "`food_id` int(11) NOT NULL, " +
                            "`count` int(11) NOT NULL, " +
                            "`new_price` int(11) NOT NULL, " +
                            "PRIMARY KEY (`food_id`), " +
                            "CONSTRAINT `food_party_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`) ON DELETE CASCADE ON UPDATE CASCADE " +
                            ")CHARACTER SET utf8 COLLATE utf8_unicode_ci;",
                    TABLE_NAME));
        } catch (SQLException ex){
            System.out.println("Error in FoodPartyFoodMapper.FoodPartyFoodMapper");
            ex.printStackTrace();
        }
    }

    @Override
    protected String getFindStatement(Integer id) {
        return "SELECT " + "F.name, F.description, F.image, F.price, F.popularity, F.res_id, S.food_id, S.count, S.new_price, R.name" +
                " FROM " + FATHER_TABLE_NAME + " F" + ", " + TABLE_NAME + " S, " + "restaurant R" +
                " WHERE S.food_id = " + id + " and F.id = " + id + " and R.id = F.res_id; ";
    }


    protected String getSaledFoodInsertStatement(SaledFood saledFood, Integer food_id) {
        return "INSERT INTO " + TABLE_NAME +
                "(" + COLUMNS + ")" + " VALUES "+
                "("+
                food_id.toString() + "," +
                saledFood.getCount() + "," +
                saledFood.getPrice() +
                ");";
    }

    @Override
    protected String getInsertStatement(SaledFood saledFood, String restaurantId){
        return "INSERT INTO " + FATHER_TABLE_NAME +
                "(" + FATHER_COLUMNS + ")" + "VALUES" +
                "(" +
                '"' + saledFood.getName() + '"' + "," +
                '"' + saledFood.getDescription() + '"' + "," +
                '"' + saledFood.getImage() + '"' + "," +
                saledFood.getOldPrice() + "," +
                saledFood.getPopularity() + "," +
                '"' + restaurantId + '"' +
                ");";
    }

    @Override
    protected String getDeleteStatement(Integer id) {
        return "DELETE FROM " + TABLE_NAME + " WHERE id = " + id + ";";
    }

    @Override
    protected SaledFood convertResultSetToObject(ResultSet rs) throws SQLException {
        SaledFood newSaledFood = null;
        try {
            newSaledFood = new SaledFood(
                    rs.getString(1),
                    rs.getString(2),
                    rs.getFloat(5),
                    rs.getInt(4),
                    rs.getString(3),
                    rs.getInt(8),
                    rs.getInt(9),
                    rs.getString(6), rs.getString(10));
            newSaledFood.setId(rs.getInt(7));

        }
        catch (SQLException e){
            System.out.println("Error in FoodPartyFoodMapper.convertResultSetToObject");
            throw e;
        }
        return newSaledFood;
    }

    @Override
    public void insert(SaledFood saledFood, String restaurantId) throws SQLException {
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement insertFoodStatement = conn.prepareStatement(getInsertStatement(saledFood, restaurantId), Statement.RETURN_GENERATED_KEYS);
            insertFoodStatement.executeUpdate();
            ResultSet resultSet = insertFoodStatement.getGeneratedKeys();
            resultSet.next();
            Integer generatedFoodId  = resultSet.getInt(1);
            PreparedStatement insertSaledFoodStatement = conn.prepareStatement(getSaledFoodInsertStatement(saledFood,generatedFoodId));
            insertSaledFoodStatement.executeUpdate();
        }
        catch (SQLException e){
            System.out.println("Error in FoodPartyFoodMapper.insert");
            throw e;
        }

    }

    public ArrayList<SaledFood> retrieve(){
        String sql = "SELECT " + "F.name, F.description, F.image, F.price, F.popularity, F.res_id, S.food_id, S.count, S.new_price, R.name" +
                " FROM " + FATHER_TABLE_NAME + " F " + ", " + TABLE_NAME + " S, " + "restaurant R" +
                " WHERE S.count > 0 and R.id = F.res_id  and F.id = S.food_id" + ";";
        ArrayList<SaledFood> foods = new ArrayList<>();
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet resultSet = st.executeQuery();
            while(resultSet.next()){
                foods.add(convertResultSetToObject(resultSet));
            }
        } catch (SQLException e){
            System.out.println("Error in FoodPartyFoodMapper.retrieve");
        }
        return foods;
    }

    public void updateFoodCount(int id,int count) throws SQLException{
        String sql = "UPDATE " + TABLE_NAME + " SET count = " + count + " WHERE food_id = " + id + ";";
        try(Connection conn = ConnectionPool.getConnection()){
            PreparedStatement st = conn.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e){
            System.out.println("Error in FoodPartyFoodMapper.updateFoodCount");
            throw e;
        }
    }
}

