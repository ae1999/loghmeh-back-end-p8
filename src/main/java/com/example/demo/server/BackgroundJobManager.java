package com.example.demo.server;


import com.example.demo.repo.RestaurantRepo;
import com.example.demo.services.Runnable.CallFoodPartyApiRunnable;
import com.example.demo.services.Runnable.CheckOrdersStatusRunnable;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@WebListener
public class BackgroundJobManager implements ServletContextListener {
    private ScheduledExecutorService checkOrdersStatusScheduler;
    private ScheduledExecutorService foodPartyCallApiScheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        RestaurantRepo.getInstance().getRestuarantsFromAPI();
        checkOrdersStatusScheduler = Executors.newSingleThreadScheduledExecutor();
        foodPartyCallApiScheduler = Executors.newSingleThreadScheduledExecutor();
        checkOrdersStatusScheduler.scheduleAtFixedRate(new CheckOrdersStatusRunnable(), 0, 10, TimeUnit.SECONDS);
        foodPartyCallApiScheduler.scheduleAtFixedRate(new CallFoodPartyApiRunnable(),0,30,TimeUnit.MINUTES);
        System.out.println("FUCK");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        foodPartyCallApiScheduler.shutdownNow();
        checkOrdersStatusScheduler.shutdownNow();
    }
}
