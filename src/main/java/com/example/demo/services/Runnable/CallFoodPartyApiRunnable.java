package com.example.demo.services.Runnable;

import com.example.demo.repo.APICaller.APICaller;
import com.example.demo.services.FoodPartyService;

public class CallFoodPartyApiRunnable implements Runnable {
    @Override
    public void run(){
        APICaller apiCaller = new APICaller();
        String json = apiCaller.getData("http://138.197.181.131:8080/foodparty");
        System.out.println(json);
        FoodPartyService.getInstance().setFoodPartyList(json);
    }
}
