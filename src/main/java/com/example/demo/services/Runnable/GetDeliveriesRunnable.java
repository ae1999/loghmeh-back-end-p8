package com.example.demo.services.Runnable;


import com.example.demo.models.Coordinate;
import com.example.demo.models.Delivery;
import com.example.demo.repo.APICaller.APICaller;
import com.example.demo.repo.OrderRepo;
import com.example.demo.utils.JsonMapper;

import java.util.List;

public class GetDeliveriesRunnable implements Runnable {
    private Coordinate userLocation;
    private Coordinate restaurantLocation;
    private OrderRepo orderRepo;
    private int orderId;

    public GetDeliveriesRunnable(Coordinate restaurantLocation,Coordinate userLocation,int orderId){
        this.restaurantLocation = restaurantLocation;
        this.userLocation = userLocation;
        this.orderRepo = OrderRepo.getInstance();
        this.orderId = orderId;
    }
    @Override
    public void run(){
        APICaller apiCaller = new APICaller();
        String deliveriesData = apiCaller.getData("http://138.197.181.131:8080/deliveries");
        System.out.println(deliveriesData);
        JsonMapper<List> listJsonMapper = new JsonMapper<>(List.class);
        List<Delivery> deliveries = listJsonMapper.readJsonList(Delivery.class, deliveriesData);
        if (deliveries.size() > 0){
            System.out.println("FUCK DELIVERY");
            this.assignBestDelivery(deliveries);
        }
    }
    private void assignBestDelivery(List<Delivery> deliveries){
        long minT = 1000000000;
        Delivery mostNearDelivery = null;
        for(Delivery d: deliveries){
            double t = (d.getLocation().findDistance(restaurantLocation)+restaurantLocation.findDistance(userLocation))/d.getVelocity();
            long roundedT = Math.round(t*1000);
            if (roundedT < minT){
                minT = roundedT;
                mostNearDelivery = d;
            }
        }
        System.out.println("FUCK 1");
        orderRepo.assignDeliveryToOrder(orderId,mostNearDelivery,minT);
    }
}
