package com.example.demo.services;

import com.example.demo.dataAccess.FoodMapper;
import com.example.demo.dataAccess.FoodpartyFoodMapper;
import com.example.demo.models.Food;
import com.example.demo.models.FoodPartyItems.FoodPartyFoodItem;
import com.example.demo.models.FoodPartyItems.FoodPartyRestaurantItem;
import com.example.demo.models.Restaurant;
import com.example.demo.models.SaledFood;
import com.example.demo.repo.RestaurantRepo;
import com.example.demo.utils.JsonMapper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FoodPartyService {
    private static FoodPartyService foodPartyService;
    private RestaurantRepo restaurantRepo;
    private FoodpartyFoodMapper foodpartyFoodMapper = FoodpartyFoodMapper.getInstance();
    private FoodMapper foodMapper = FoodMapper.getInstance();
    public static FoodPartyService getInstance(){
        if (foodPartyService == null){
            foodPartyService = new FoodPartyService();
        }
        return foodPartyService;
    }

    private FoodPartyService(){
        restaurantRepo = RestaurantRepo.getInstance();
    }

    public void setFoodPartyList(String foodPartyJson){
        ArrayList<SaledFood> foodPartyList = new ArrayList<SaledFood>();
        JsonMapper<List> listJsonMapper = new JsonMapper<List>(List.class);
        List<FoodPartyRestaurantItem> items = listJsonMapper.readJsonList(FoodPartyRestaurantItem.class,foodPartyJson);
        for(FoodPartyRestaurantItem item : items){
            Restaurant restaurant = restaurantRepo.searchRestaurantById(item.id);
            if(restaurant == null) {
                restaurantRepo.addRestaurant(new Restaurant(item.id, item.name, item.location, item.logo));
            }
            List<FoodPartyFoodItem> foodPartyFoods = item.menu;
            for (FoodPartyFoodItem foodItem : foodPartyFoods) {
                this.addFoodToFoodParty(
                        new SaledFood(foodItem.name, foodItem.description, foodItem.popularity, foodItem.oldPrice, foodItem.image, foodItem.count, foodItem.price, item.id, item.name),
                        item.id
                );
            }
        }
    }

    public ArrayList<SaledFood> getFoodPartyList() {
        return foodpartyFoodMapper.retrieve();
    }

    private void addFoodToFoodParty(SaledFood saledFood, String  restaurantId){
        try{
            foodpartyFoodMapper.insert(saledFood,restaurantId);
        } catch(SQLException e){
            e.printStackTrace();
        }

    }


    public SaledFood searchFood(String restaurantId, String foodName){
        SaledFood partyFood = null;
        try{
            Food food = foodMapper.findFoodByName(foodName,restaurantId);
            if (food!=null)
                partyFood = foodpartyFoodMapper.find(food.getId());
        } catch (SQLException e){
            e.printStackTrace();
        }
        return partyFood;
    }

    public void updateFoodPartyFoodCount(int id, int count){
        try{
            foodpartyFoodMapper.updateFoodCount(id,count);
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

}
