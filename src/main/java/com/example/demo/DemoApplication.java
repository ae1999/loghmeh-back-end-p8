package com.example.demo;

import com.example.demo.dataAccess.UserMapper;
import com.example.demo.services.UserDetailsServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@ServletComponentScan
@SpringBootApplication
public class DemoApplication {

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	@Bean
	public UserDetailsServiceImpl userDetailsService() { return new UserDetailsServiceImpl(UserMapper.getInstance());}

	public static void main(String[] args) {
		System.setProperty("server.port", "8081");
		SpringApplication.run(DemoApplication.class, args);
	}

}
