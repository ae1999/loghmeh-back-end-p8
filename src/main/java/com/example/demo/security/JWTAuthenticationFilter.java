package com.example.demo.security;

import com.auth0.jwt.JWT;
import com.example.demo.models.RequestModel.UserCredentials;
import com.example.demo.services.UserDetailsServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC256;
import static com.example.demo.security.SecurityConstants.*;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private UserDetailsServiceImpl userDetailsService;

    protected JWTAuthenticationFilter(AuthenticationManager authenticationManager){
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response ) throws AuthenticationException {
        try{
            String password;
            UserCredentials userCredentials = new ObjectMapper().readValue(request.getInputStream(), UserCredentials.class);
            if (userCredentials.getPassword().equals("")) {
                UserDetails user = userDetailsService.loadUserByUsername(userCredentials.getEmail());
                password = user.getPassword();
            }
            else password = userCredentials.getPassword();
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userCredentials.getEmail(),
                            password,
                            new ArrayList<>()
                    )
            );
        } catch (IOException e){
            throw new RuntimeException(e); //TODO make a new exception Handler;
        }
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) throws IOException, ServletException {
        String token = JWT.create()
                .withSubject(((User) auth.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC256(SECRET.getBytes()));
//        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        String ali = TOKEN_PREFIX + token;
        String newContent = "{ \"token\" : \"" + ali + "\" }";
        response.setContentLength(newContent.length());
        System.out.println(newContent);
        response.getWriter().write(newContent);
    }
}
